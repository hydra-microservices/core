module.exports = {
  roots: [
    "<rootDir>/src/",
    "<rootDir>/tests/",
  ],
  collectCoverageFrom: [
    "src/**/*.{ts,js,tsx,jsx}"
  ],
  coverageReporters: [
    "json",
    "lcov",
    "text",
    "clover",
    "cobertura"
  ],
  testMatch: [
    "**/tests/**/*.spec.(ts|js)"
  ],
  preset: 'ts-jest',
  testEnvironment: 'node',
};