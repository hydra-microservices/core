import {HydraMessageData, HydraMessageDataTypes, HydraMessagePriority, HydraMessageSerializedData, serialize as serializeMessageData} from "./data";
export {HydraMessageData, HydraMessageDataTypes, HydraMessagePriority, HydraMessageSerializedData};
import {HydraMessageDataChecksum} from "./checksum"
export {HydraMessageDataChecksum}

/**
 *
 */
export abstract class HydraMessage<DataType extends HydraMessageDataTypes> {
    protected abstract _data: HydraMessageData<DataType>;

    /**
     * 
     */
    public get rawData(): HydraMessageData<DataType> {
        return this._data;
    }

    /**
     * 
     */
    public get rawSerializedData(): HydraMessageSerializedData<DataType> {
        return serializeMessageData(this._data)
    }

    /**
     * 
     */
    public get rawJsonData(): string {
        return JSON.stringify(this.rawSerializedData);
    }

    /**
     * 
     */
    public get valid(): boolean {
        return HydraMessageDataChecksum.checkChecksum(this._data);
    }
    
    /**
     * 
     */
    public calculateChecksum(): string {
        this._data.checksum = HydraMessageDataChecksum.calculateChecksum(this._data);
        return this._data.checksum;
    } 
}
