/**
 *
 */
 export type HydraMessageDataTypes = Buffer | object;

 /**
  *
  */
 export enum HydraMessagePriority {
     extremlylow = 0,
     verylow = 1,
     low = 2,
     normal = 3,
     moderate = 4,
     high = 5,
     moderatelyhigh = 6,
     veryhigh = 7,
     extremlyhigh = 8,
     highest = 9,
 }
 
 /**
  *
  */
 export interface HydraMessageData<DataType extends HydraMessageDataTypes> {
     from: string;
     to: string;
     proxy?: string;
     messageId: string;
     replyToMessageId?: string;
     priority: HydraMessagePriority;
     dataType: DataType extends Buffer ? "buffer" : "object";
     data: DataType;
     timestamp?: string;
     authorization?: string;
     trace: string[];
     checksum?: string;
     signature?: string
 }
 
 /**
  *
  */
 export interface HydraMessageSerializedData<DataType extends HydraMessageDataTypes> extends Omit<HydraMessageData<DataType>, "dataType" | "data"> {
     data: DataType extends Buffer ? string : Exclude<DataType, Buffer>;
 }

 export function serialize<DataType extends HydraMessageDataTypes>(data: HydraMessageData<DataType>): HydraMessageSerializedData<DataType> {
    return { ...data, data: (data.dataType === "buffer" ? data.data.toString("base64") : data.data) as DataType extends Buffer ? string : Exclude<DataType, Buffer> };
 }