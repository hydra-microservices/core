import { HydraMessageData, HydraMessageDataTypes, serialize as serializeMessageData} from "./data";
import crypto from "crypto";

/**
 * 
 */
export class HydraMessageDataChecksum {
    public static calculateChecksum(data: HydraMessageData<HydraMessageDataTypes>): string {
        return crypto.createHash("md5").update(JSON.stringify(serializeMessageData((({checksum,signature, ...o})=>o)(data))), "utf8").digest("hex");
    }
    public static checkChecksum(data: HydraMessageData<HydraMessageDataTypes>, withoutChecksum = false): boolean {
        return data.checksum == null? withoutChecksum: data.checksum === this.calculateChecksum(data);
    }
    public static copyWithChecksum<DataType extends HydraMessageDataTypes>(data: HydraMessageData<DataType>): HydraMessageData<DataType> {
        return {...data, checksum: this.calculateChecksum(data)}
    }
}